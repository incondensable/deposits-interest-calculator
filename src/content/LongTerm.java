package content;

import util.IntegerUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LongTerm extends Deposit {

    public static final int INTEREST_RATE = 20;

    public LongTerm() {
    }

    public LongTerm(String customerNumber, String depositType, BigDecimal depositBalance, Integer durationInDays) {
        super(customerNumber, depositType, depositBalance, durationInDays);
    }

    @Override
    public BigDecimal calculatePayedInterest() {
        return BigDecimal.valueOf(INTEREST_RATE)
                .multiply(super.getDepositBalance())
                .multiply(BigDecimal.valueOf(super.getDurationInDays()))
                .divide(BigDecimal.valueOf(IntegerUtil.DIVISION), 0, RoundingMode.HALF_UP);
    }
}
