package convertor;

import content.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static validation.ValidationClass.*;

public class XMLConvertor implements Serializable {

    public static List<Deposit> getXMLInvalid() {
        List<Deposit> invalidList = new ArrayList<>();
        Class<?> cl = Deposit.class;
        Deposit deposit;

        try {
            File input = new File("resource\\Deposits.txt");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = dBuilder.parse(input);
            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("deposit");

            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                Node node = nodeList.item(temp);
                Element element = (Element) node;

                String type = element.getElementsByTagName("depositType").item(0).getTextContent();
                Integer days = new Integer(element.getElementsByTagName("durationInDays").item(0).getTextContent());
                BigDecimal balance = new BigDecimal(element.getElementsByTagName("depositBalance").item(0).getTextContent());
                String customerNumber = element.getElementsByTagName("customerNumber").item(0).getTextContent();

                if (!(depositBalanceValidation(balance) && depositDaysValidation(days) && depositTypeValidation(type))) {
                    deposit = new Deposit();
                    deposit.setCustomerNumber(customerNumber);
                    invalidList.add(deposit);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        invalidList.sort(Comparator.comparing(Deposit::getCustomerNumber).reversed());
        return invalidList;
    }

    public static List<Deposit> getXML() {
        List<Deposit> depositList = new ArrayList<>();
        Class<?> cl = Deposit.class;
        Deposit deposit;

        try {
            File input = new File("resource\\Deposits.txt");
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = dBuilder.parse(input);
            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("deposit");

            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                Node node = nodeList.item(temp);
                Element element = (Element) node;

                String type = element.getElementsByTagName("depositType").item(0).getTextContent();
                Integer days = new Integer(element.getElementsByTagName("durationInDays").item(0).getTextContent());
                BigDecimal balance = new BigDecimal(element.getElementsByTagName("depositBalance").item(0).getTextContent());
                String customerNumber = element.getElementsByTagName("customerNumber").item(0).getTextContent();

                if (depositBalanceValidation(balance) && depositDaysValidation(days) && depositTypeValidation(type)) {
                    switch (type) {
                        case "Qarz":
                            deposit = new Qarz();
                            depositList.add(validDeposit(cl, deposit, customerNumber, type, days, balance));
                            break;
                        case "ShortTerm":
                            deposit = new ShortTerm();
                            depositList.add(validDeposit(cl, deposit, customerNumber, type, days, balance));
                            break;
                        case "LongTerm":
                            deposit = new LongTerm();
                            depositList.add(validDeposit(cl, deposit, customerNumber, type, days, balance));
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        depositList.sort(Comparator.comparing(Deposit::calculatePayedInterest).reversed());
        return depositList;
    }

    public static Deposit validDeposit(Class<?> cl, Deposit deposit, String customerNumber, String type, Integer days, BigDecimal balance) {
        try {
            Method methodSetCustomerNumber = cl.getDeclaredMethod("setCustomerNumber", String.class);
            Method methodSetDepositType = cl.getDeclaredMethod("setDepositType", String.class);
            Method methodSetDepositBalance = cl.getDeclaredMethod("setDepositBalance", BigDecimal.class);
            Method methodSetDurationInDays = cl.getDeclaredMethod("setDurationInDays", Integer.class);

            methodSetCustomerNumber.invoke(deposit, customerNumber);
            methodSetDepositType.invoke(deposit, type);
            methodSetDepositBalance.invoke(deposit, balance);
            methodSetDurationInDays.invoke(deposit, days);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return deposit;
    }

//    public static List<Deposit> getXML() {
//        List<Deposit> depositList = new ArrayList<>();
//        Class<?> cl = Deposit.class;
//        Deposit deposit;
//
//        try {
//            File input = new File("resource\\Deposits.txt");
//            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder dBuilder = documentBuilderFactory.newDocumentBuilder();
//            Document document = dBuilder.parse(input);
//            document.getDocumentElement().normalize();
//
//            NodeList nodeList = document.getElementsByTagName("deposit");
//
//            for (int temp = 0; temp < nodeList.getLength(); temp++) {
//                Node node = nodeList.item(temp);
//                Element element = (Element) node;
//
//                String type = element.getElementsByTagName("depositType").item(0).getTextContent();
//                Integer days = new Integer(element.getElementsByTagName("durationInDays").item(0).getTextContent());
//                BigDecimal balance = new BigDecimal(element.getElementsByTagName("depositBalance").item(0).getTextContent());
//                String customerNumber = element.getElementsByTagName("customerNumber").item(0).getTextContent();
//
//                if (depositBalanceValidation(balance) && depositDaysValidation(days) && depositTypeValidation(type)) {
//                    switch (type) {
//                        case "Qarz":
//                            deposit = new Qarz();
//                            depositList.add(validDeposit(cl, deposit, customerNumber, type, days, balance));
//                            break;
//                        case "ShortTerm":
//                            deposit = new ShortTerm();
//                            depositList.add(validDeposit(cl, deposit, customerNumber, type, days, balance));
//                            break;
//                        case "LongTerm":
//                            deposit = new LongTerm();
//                            depositList.add(validDeposit(cl, deposit, customerNumber, type, days, balance));
//                            break;
//                    }
//                } else {
//                    deposit = new Deposit();
//                    deposit.setCustomerNumber(customerNumber);
//                    System.out.println(deposit.getCustomerNumber() + "'s data is invalid");
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        depositList.sort(Comparator.comparing(Deposit::calculatePayedInterest).reversed());
//        return depositList;
//    }
//
//    public static Deposit validDeposit(Class<?> cl, Deposit deposit, String customerNumber, String type, Integer days, BigDecimal balance) {
//        try {
//            Method methodSetCustomerNumber = cl.getDeclaredMethod("setCustomerNumber", String.class);
//            Method methodSetDepositType = cl.getDeclaredMethod("setDepositType", String.class);
//            Method methodSetDepositBalance = cl.getDeclaredMethod("setDepositBalance", BigDecimal.class);
//            Method methodSetDurationInDays = cl.getDeclaredMethod("setDurationInDays", Integer.class);
//
//            methodSetCustomerNumber.invoke(deposit, customerNumber);
//            methodSetDepositType.invoke(deposit, type);
//            methodSetDepositBalance.invoke(deposit, balance);
//            methodSetDurationInDays.invoke(deposit, days);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return deposit;
//    }
}