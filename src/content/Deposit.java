package content;

import java.math.BigDecimal;

public class Deposit {

    private String customerNumber;
    private String depositType;
    private BigDecimal depositBalance;
    private Integer durationInDays;

    public Deposit() {
    }

    public Deposit(String customerNumber, String depositType, BigDecimal depositBalance, Integer durationInDays) {
        this.customerNumber = customerNumber;
        this.depositType = depositType;
        this.depositBalance = depositBalance;
        this.durationInDays = durationInDays;
    }

    public BigDecimal calculatePayedInterest() {
        return BigDecimal.ZERO;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getDepositType() {
        return depositType;
    }

    public void setDepositType(String depositType) {
        this.depositType = depositType;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public void setDepositBalance(BigDecimal depositBalance) {
        this.depositBalance = depositBalance;
    }

    public Integer getDurationInDays() {
        return durationInDays;
    }

    public void setDurationInDays(Integer durationInDays) {
        this.durationInDays = durationInDays;
    }

    public String toString() {
            return ("Customer Number: " + customerNumber
                    + " | Deposit Type: " + depositType
                    + " | Deposit Balance: " + depositBalance
                    + " | Duration: " + durationInDays
                    + " | and Interest of: " + calculatePayedInterest());
    }
}
