package content;

import java.math.BigDecimal;

public class Qarz extends Deposit {

    public Qarz() { }

    @Override
    public BigDecimal calculatePayedInterest() {
        return BigDecimal.ZERO;
    }
}
