package output;

import content.Deposit;
import convertor.XMLConvertor;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

public class FileOutput implements Serializable {
    public static void validDataOutput() {
        try {
            FileWriter fileWriter = new FileWriter("output.txt");
            Map<BigDecimal, String> result = new TreeMap<>(Collections.reverseOrder());
            StringBuilder sb = new StringBuilder();
            List<String> customerNumbers = new ArrayList<>();
            List<Deposit> invalidDepositsList = XMLConvertor.getXMLInvalid();
            List<Deposit> depositList = XMLConvertor.getXML();

            for (int i = depositList.size() - 1; i >= 0; i--) {
                result.put(depositList.get(i).calculatePayedInterest(), depositList.get(i).getCustomerNumber());
            }

            for (Map.Entry<BigDecimal, String> entry : result.entrySet()) {
                sb.append("Customer Number is: ").append(entry.getValue()).append(" and the interest paid is: " + entry.getKey() + "\n");
            }

            for (int i = invalidDepositsList.size() - 1; i >= 0; i--) {
                customerNumbers.add(invalidDepositsList.get(i).getCustomerNumber());
            }

            for (String s : customerNumbers) {
                sb.append(s).append("'s data is invalid\n");
            }

            fileWriter.write(String.valueOf(sb));
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}